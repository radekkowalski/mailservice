﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MailService
{
    public class MailSender
    {
        public void SendEmail(string receiverMail, string subject, string message, bool isHtml = false , byte[] attachmentStream = null, string attachmentName = "")
        {
            try
            {
                MailAddress sender = new MailAddress("multiloans@serwer18803.lh.pl", "Multiloans");
                MailAddress receiver = new MailAddress(receiverMail);

                MailMessage mail = new MailMessage(sender, receiver);

                SmtpClient client = new SmtpClient();

                NetworkCredential credentials = new NetworkCredential("multiloans@serwer18803.lh.pl", "RrrKks2017");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                client.Port = 465;
                client.Host = "mail-serwer18803.lh.pl";

                client.EnableSsl = true;
                mail.Subject = subject;
                mail.Body = message;

                if (isHtml)
                {
                    mail.IsBodyHtml = true;
                }

                if (attachmentStream != null)
                {
                    ProcessMailAttachmentOperations(mail, attachmentStream, attachmentName);
                }

                client.Send(mail);
            }
            catch (SmtpFailedRecipientException ex1)
            {
                var m = ex1.Message;
            }
            catch (SmtpException ex)
            {
                var m = ex.Message;
            }
            catch (Exception ex2)
            {
                var m = ex2.Message;
            }
        }

        private MailMessage ProcessMailAttachmentOperations(MailMessage mail, byte[] attachmentSteam, string attachmentName)
        {
            Attachment attachment = new Attachment(new MemoryStream(attachmentSteam), attachmentName);
            mail.Attachments.Add(attachment);

            return mail;
        }
    }
}
